###############################
# Cambio de banda de colores en una imagen.
# Autores: Mendoza, Dante – Dejean, Gustavo.
# Fecha: 18/07/2021
###############################
	
library(raster)
library(stringr)
	 
setwd(paste0(getwd(),"/images" ))
dir("original")
vec <- list.files(path="original", pattern='jpg')
	 
for (i in 1:length(vec)) {
  raster <- brick(paste0("original/",vec[i]))
  names(raster) <- c("Red","Green", "Blue" )
  red      <-  raster$Red      
  green    <-  raster$Green
  blue     <-  raster$Blue  
	 
# creo indices
# GLI Green leaf index
GLI    <-  (2 * green - red -  blue )/ (2 * green + red +  blue + 0.000001)
  raster$Blue <- round((GLI +1 ) * 127.5)
 
  jpeg(filename= paste0("transformed/", vec[i]),
       width= nrow(raster),
       height= ncol(raster),
       quality = 100)
  plotRGB(raster)
  dev.off()
  }
